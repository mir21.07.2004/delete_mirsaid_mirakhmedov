DELETE FROM rental
WHERE inventory_id IN (SELECT inventory_id FROM inventory WHERE film_id = (SELECT film_id FROM film WHERE title = 'Stardust'));

DELETE FROM rental
WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Ilkhom' AND last_name = 'Kodirov');

DELETE FROM inventory
